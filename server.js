const express = require('express');
const path = require('path');
const socketIO = require('socket.io');
const mysql = require('mysql');


const app= express();

//Agregar headers
app.use(function(request, result, next){
    result.setHeader("Access-Control-Allow-Origin", "*");
    next();
});


const dbconnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'messages'
    });

dbconnection.connect(function(error){
    //muestra errores
});

app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));


const server=app.listen(app.get('port'), () =>{
    console.log('Servidor corriendo en el puerto: ',app.get('port'));
});

const io=socketIO(server);

io.on('connection',(socket)=>{
    socket.on('message', function(data){
        dbconnection.query("insert into message values(null, '"+data+"');",function(error,result){
            dbconnection.query("select * from message join (select max(id)as maxid from message)as tabla where message.id=tabla.maxid;", function(err, messages){
                io.emit('message',{
                    id: messages[0].id,
                    message: messages[0].message
                })
            });
        }); 
    });

    socket.on('id-eliminar', function(data){
        dbconnection.query("delete from message where id='"+data+"';",function(error,result){
            io.sockets.emit('id-eliminar',data);
        }); 
    });
    
});

//crear API para obtener los mensajes
app.get("/get_messages", function(request,result){
    dbconnection.query("select * from message;", function(error, messages){
        //retornar los datos en un formato JSON
        result.end(JSON.stringify(messages));
    });
});
app.get('/',function(request,result){
    result.send("Hello world");
});