const socket = io();



function sendMessage(){
    var message = document.getElementById('message');
    socket.emit("message",message.value);
    return false;
}

var server="http://localhost:3000";
$.ajax({
    url: server +"/get_messages",
    method: "GET",
    success: function (response){
        var messages = document.getElementById('messages');
        var data = JSON.parse(response);
        for(var i=0; i<data.length; i++){
            messages.innerHTML +=`<li id='li-${data[i].id}'>${data[i].message}<button id='${data[i].id}' onclick='deleteMessage(this);'>Eliminar</button></li>`;
        }
    }
});


socket.on("message", function(data){
    var messages = document.getElementById('messages');
    messages.innerHTML += `<li id='li-${data.id}'>${data.message}<button id='${data.id}' onclick='deleteMessage(this);'>Eliminar</button></li>`;

});

function deleteMessage(self){
    var id = self.getAttribute('id');
    socket.emit('id-eliminar',id);
}

socket.on("id-eliminar", function(data){
    const li= document.getElementById('li-'+data);
    li.innerHTML = "Este mensaje ah sido eliminado!!";
})